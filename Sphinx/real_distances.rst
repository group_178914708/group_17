real_distances Module
=================

The `real_distances` module is responsible for defining the data structures and models used in the project.

.. automodule:: real_distances
   :members:
   :undoc-members:
   :show-inheritance:
