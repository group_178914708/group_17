Welcome to Group_17 Adpro Project's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   showcasenotebook
   data_model
   real_distances
   test_gaivota

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
