Data Model Module
=================

The `data_model` module is responsible for defining the data structures and models used in the project.

.. automodule:: data_model
   :members:
   :undoc-members:
   :show-inheritance:
